# WeatherApp

Hi! This is what I have been able to make during this week. 
![
](https://lh3.googleusercontent.com/li4svAJdYkwBX-Ri7w69OH31ptngUCuavTIKziWkQm4o_RfuLPW2lwFVVkZNYoQ74-a2KHagsxFs "Gui")

## Features TODO list

- [x] Fetching weather data
- [x] Fetching 5 day forecast
- [x] Displaying temperature chart
- [x] Displaying ugly weather icons
- [x] Displaying brief forecast for 5 days
- [x] Using closest match to fetch weather data
- [x] Unit Tests
- [x] Exception Handling
- [ ] Chart improvements
- [ ] Dumping wethear data to file
- [ ] Implement MVVM architechture
- [ ] Change ugly GUI

## How to run?

There are two options to run this app on Windows

1. Run pre-built .exe file from bin/release directory
2. Build project yourself and run .exe file that has been built.
