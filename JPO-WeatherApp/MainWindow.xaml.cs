﻿using System;
using System.Linq;
using System.Net.Http;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using JPO_WeatherApp.Domain;
using LiveCharts;
using LiveCharts.Helpers;
using LiveCharts.Wpf;

namespace JPO_WeatherApp
{
    public partial class MainWindow : Window
    {
        private readonly WeatherService service;
        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> YFormatter { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            var client = new HttpClient();
            service = new WeatherService(client);

            SeriesCollection = new SeriesCollection
            {
                new LineSeries { Title = "Temperature"}
            };
            
            YFormatter = value => value.ToString("F1");
            DataContext = this;
            DisplayWeather();
        }
        
        
        private void DisplayWeather()
        {
            try
            {
                var city = CityTextBox.Text == "" ? "Krakow" : CityTextBox.Text;
                var weatherForecast = service.GetWeatherForecastByCityName(city);
                
                DisplayDailyWeather(weatherForecast);
                DisplayDailyTemperatureChart(weatherForecast);
            }
            catch (Exception e)
            {
                Message.Text = e.Message;
            }
        }

        private void DisplayDailyWeather(Forecast forecast)
        {
            var currentDate = DateTime.Today;

            Date0.Text = currentDate.DayOfWeek.ToString();
            Temperature0.Text = forecast.GetTemperatureForSingleDay(0);
            Image0.Source = new BitmapImage(new Uri(forecast.GetImageSourceForSingleDay(0)));

            Date1.Text = currentDate.AddDays(1).DayOfWeek.ToString();
            Temperature1.Text = forecast.GetTemperatureForSingleDay(1);
            Image1.Source = new BitmapImage(new Uri(forecast.GetImageSourceForSingleDay(1)));

            Date2.Text = currentDate.AddDays(2).DayOfWeek.ToString();
            Temperature2.Text = forecast.GetTemperatureForSingleDay(2);
            Image2.Source = new BitmapImage(new Uri(forecast.GetImageSourceForSingleDay(2)));

            Date3.Text = currentDate.AddDays(3).DayOfWeek.ToString();
            Temperature3.Text = forecast.GetTemperatureForSingleDay(3);
            Image3.Source = new BitmapImage(new Uri(forecast.GetImageSourceForSingleDay(3)));

            Date4.Text = currentDate.AddDays(4).DayOfWeek.ToString();
            Temperature4.Text = forecast.GetTemperatureForSingleDay(4);
            Image4.Source = new BitmapImage(new Uri(forecast.GetImageSourceForSingleDay(4)));
        }

        private void DisplayDailyTemperatureChart(Forecast forecast)
        {
            SeriesCollection[0].Values = forecast.list.Select(data => data.Main.Temp).Take(8).AsChartValues();
            Labels = forecast.list.Select(data => data.Dt_Txt.Split()[1]).Take(8).ToArray();
        }

        private void CityComboBox_OnLostFocus(object sender, RoutedEventArgs e)
        {
            DisplayWeather();
        }

        private void CityComboBox_GotFocus(object sender, RoutedEventArgs e)
        {
            DisplayWeather();
        }

        private void CityComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return || e.Key == Key.Enter)
                DisplayWeather();
        }
    }
}
