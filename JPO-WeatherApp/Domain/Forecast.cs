﻿using System;
using System.Collections.Generic;
using System.Linq;
using JPO_WeatherApp.Model;

namespace JPO_WeatherApp.Domain
{
    public class Forecast
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public City City { get; set; }
        public int Cnt { get; set; }
        public List<WeatherData> list { get; set; }

        public string GetTemperatureForSingleDay(int dayNumber)
        {
            return dayNumber == 0 ? 
                GetCurrentWeatherData().Main.Temp.ToString("#") :
                GetWeatherDataForDay(dayNumber).Main.Temp.ToString("#");
        }

        public string GetImageSourceForSingleDay(int dayNumber)
        {
            return $"http://openweathermap.org/img/w/{GetImageCodeForSingleDay(dayNumber)}.png";
        }

        public string GetImageCodeForSingleDay(int dayNumber)
        {
            return dayNumber == 0 ? GetCurrentWeatherData().weather.First().Icon : GetWeatherDataForDay(dayNumber).weather.First().Icon;
        }

        private WeatherData GetCurrentWeatherData()
        {
            return list.First(weatherData => DateTimeUtils.GetDate(weatherData.Dt).Date == DateTime.Today.Date);
        }

        private WeatherData GetWeatherDataForDay(int dayNumber)
        {
            return list.First(weatherData =>
                DateTimeUtils.GetDate(weatherData.Dt) == DateTime.Today.AddDays(dayNumber).AddHours(1));
        }
    }
}