﻿using System;
using System.Collections.Generic;
using System.IdentityModel;
using System.Net.Http;
using Newtonsoft.Json;

namespace JPO_WeatherApp.Domain
{
    public class WeatherService
    {
        private readonly HttpClient client;
        private const string AppId = "61a321b0e01521af99297d05861b1a3c";

        public WeatherService(HttpClient client)
        {
            this.client = client;
            client.BaseAddress = new Uri("https://api.openweathermap.org/data/2.5/");
        }

        public Forecast GetWeatherForecastByCityName(string cityName)
        {

            var httpResponse = client.GetAsync($"forecast?q={cityName}&type=like&appid={AppId}&units=metric");

            if (!httpResponse.Result.IsSuccessStatusCode)
                throw new AsynchronousOperationException(
                    $"API call failed with status code: {httpResponse.Result.StatusCode}");

            var json = httpResponse.Result.Content.ReadAsStringAsync().Result;
            var data = JsonConvert.DeserializeObject<Forecast>(json);
            return data;
        }

        public Forecast GetWeatherForecastById(int id)
        {
            var httpResponse = client.GetAsync($"forecast?id={id}&appid={AppId}&units=metric");

            if (!httpResponse.Result.IsSuccessStatusCode)
                throw new AsynchronousOperationException(
                    $"API call failed with status code: {httpResponse.Result.StatusCode}");

            var json = httpResponse.Result.Content.ReadAsStringAsync().Result;
            var data = JsonConvert.DeserializeObject<Forecast>(json);
            return data;
        }
    }
}
