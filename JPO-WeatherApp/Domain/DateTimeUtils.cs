﻿using System;

namespace JPO_WeatherApp.Domain
{
    public class DateTimeUtils
    {
        public static DateTime GetDate(double millis)
        {
            if (millis < 0)
                throw new ArgumentOutOfRangeException("millis");

            var day = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).ToLocalTime();
            return day.AddSeconds(millis).ToLocalTime();
        }
    }
}
