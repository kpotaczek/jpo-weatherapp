﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JPO_WeatherApp.Domain;

namespace WeatherAppTest
{
    [TestClass]
    public class DateTimeUtilsTests
    {
        [TestMethod]
        public void GetDateReturnsDateTimeInUTC()
        {
            var result = DateTimeUtils.GetDate(1);

            Assert.AreEqual(new DateTime(1970, 1, 1, 0, 0, 1, 0, DateTimeKind.Utc).ToLocalTime(), result);
        }

        [TestMethod]
        public void GetDateThrowsAnException()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() =>DateTimeUtils.GetDate(-1));
        }
    }
}
