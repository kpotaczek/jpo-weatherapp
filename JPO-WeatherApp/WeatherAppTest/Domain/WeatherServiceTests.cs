﻿using System;
using System.IdentityModel;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JPO_WeatherApp.Domain;
using Moq;
using Moq.Protected;

namespace WeatherAppTest
{
    [TestClass]
    public class WeatherServiceTests
    {


        [TestMethod]
        public void ThrowsExceptionWhenAPICallFails()
        {
            var expectedResponse = "Response text";

            var mockResponse = new HttpResponseMessage(HttpStatusCode.BadRequest);
            var mockHandler = new Mock<HttpClientHandler>();
            mockHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .Returns(Task.FromResult(mockResponse));

            var service = new WeatherService(new HttpClient(mockHandler.Object));
            Assert.ThrowsException<AsynchronousOperationException>(() => service.GetWeatherForecastByCityName("Test"));
        }
    }
}