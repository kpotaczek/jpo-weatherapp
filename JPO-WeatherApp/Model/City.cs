﻿namespace JPO_WeatherApp.Model
{
    public class City
    {
        public int id { get; set; }
        public string name { get; set; }
        public string country { get; set; }
        public Coordinates coord { get; set; }
    }
}