﻿namespace JPO_WeatherApp.Model
{
    public class Coordinates
    {
        public double lon { get; set; }
        public double lat { get; set; }

        public Coordinates(double lon, double lat)
        {
            this.lon = lon;
            this.lat = lat;
        }
    }
}