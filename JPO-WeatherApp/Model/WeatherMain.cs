﻿namespace JPO_WeatherApp.Model
{
    public class WeatherMain
    {
        public double Temp { get; set; }
        public double TempMin { get; set; }
        public double TempMax { get; set; }
        public double Pressure { get; set; }
        public double Humidity { get; set; }
    }
}