﻿using System.Collections.Generic;

namespace JPO_WeatherApp.Model
{
    public class WeatherData
    {
        public double Dt { get; set; }
        public WeatherMain Main { get; set; }
        public List<WeatherDescription> weather { get; set; }
        public string Dt_Txt { get; set; }
    }
}